from student import Student


class Classe:
        def __init__(self,name) -> None:
         self.name=name
         self.students= []
        
        def save(self):
            pass
        
        def addStudents(self,student)-> list [Student]:
            if not self.is_get_id(student.number):
                return self.students.append(student)
            else:
                raise Exception(f"Student with ID {student.number} already exists in the class.")

        def get_taille_classe(self):
            return len(self.students)
        
        def is_get_id(self,id):
            for student1 in self.students:
                if student1.number == id:
                    return True
            return False
        