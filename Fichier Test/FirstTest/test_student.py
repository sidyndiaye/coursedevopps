
import pytest
from classe import Classe
from student import Student


def test_Something():
    a = 1
    assert a == 1

@pytest.fixture()
def fix_add():
    B3GL = Classe("B3GL")
    
    daniel = Student()
    daniel.name = "daniel"
    daniel.number = 12
    
    modou = Student()
    modou.name = "modou"
    modou.number = 11
    
    initSize = B3GL.get_taille_classe()
    
    B3GL.addStudents(daniel)
    B3GL.addStudents(modou)
    
    
    return B3GL,initSize
    
    
def test_addStudents(fix_add):
    B3GL,init_size = fix_add
    assert B3GL.get_taille_classe() > init_size
    
    
def test_checkStudentNumber(fix_add):
    B3GL, _ = fix_add
    simon = Student()
    simon.name = "daniel"
    simon.number = 13
    
    B3GL.addStudents(simon)
    assert B3GL.is_get_id(11) == True

    

    